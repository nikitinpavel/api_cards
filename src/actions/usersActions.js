import { startSetCards } from '../actions/cardsActions';

//generate unique strind for id
const uuid = require('uuid/v1');


// SET_USERS
export const setUsers = (users) => ({
  type: 'SET_USERS',
  users
});

export const startSetUsers = () => {
  return (dispatch, getState) => {
    //create sessionStorage item from users state if it doesn't exist
    const stateUsers = getState().users;

    if (!sessionStorage.getItem('users')) {
      sessionStorage.setItem('users', JSON.stringify(stateUsers));
    }

    //get users data from sessionStorage and dispatch it
    const users = JSON.parse(sessionStorage.getItem('users'));
    dispatch(setUsers(users));
  };
};


// ADD_USER
export const addUser = (user) => ({
  type: 'ADD_USER',
  user
});

export const startAddUser = (userData = {}) => {
  return (dispatch, getState) => {
    const userId = uuid();
    const {
      firstName = '',
      lastName = '',
      about = ''
    } = userData;

    const user = {
      id: userId,
      firstName,
      lastName,
      about
    };


    //get users data from sessionStorage
    const storageUsers = JSON.parse(sessionStorage.getItem('users'));

    const { users } = storageUsers;

    users.push(user);

    //save new users data in sessionStorage
    sessionStorage.setItem('users', JSON.stringify(storageUsers));

    dispatch(addUser(user));

  };
};



// SELECT_USER
export const selectUser = (id) => ({
  type: 'SELECT_USER',
  id
});

export const startSelectUser = (id, location, history) => {
  return (dispatch, getState) => {

    const users = JSON.parse(sessionStorage.getItem('users'));
    //check if the user is already selected
    if (users.userActive !== id) {
      //id - this is id of selected user
      users.userActive = id;

      //save new users data in sessionStorage
      sessionStorage.setItem('users', JSON.stringify(users));

      dispatch(selectUser(id));
      //mix cards with user select action
      dispatch(startSetCards());

      //if user on CardPage make redirection on DashPage
      if (location.pathname.search(/card/i) !== -1) {
        history.push('/');
      }

    }
  };
};


// EDIT_USER
export const editUser = (user, userId) => ({
  type: 'EDIT_USER',
  userId,
  user
});

export const startEditUser = (user) => {
  return (dispatch, getState) => {

    const users = JSON.parse(sessionStorage.getItem('users'));

    const userId = users.userActive;

    //Find index of specific object using findIndex method.
    const userIndex = users.users.findIndex((user => user.id === userId));

    //Update edited user in storage.
    users.users[userIndex] = {
      ...user,
      id: userId
    };

    //save new users data in sessionStorage
    sessionStorage.setItem('users', JSON.stringify(users));

    dispatch(editUser(user, userId));
  };
};

// REMOVE_USER
export const removeUser = ({ userId } = {}) => ({
  type: 'REMOVE_USER',
  userId
});

export const startRemoveUser = () => {
  return (dispatch, getState) => {

    const users = JSON.parse(sessionStorage.getItem('users'));

    const userId = users.userActive;

    //Find index of specific object using findIndex method.
    const userIndex = users.users.findIndex((user => user.id === userId));

    //Remove user in storage.
    users.users.splice(userIndex, 1);

    //save new users data in sessionStorage
    sessionStorage.setItem('users', JSON.stringify(users));

    dispatch(removeUser({ userId }));

  };
};