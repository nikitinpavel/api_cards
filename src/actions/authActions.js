//generate unique strind for token
const uuid = require('uuid/v1');

export const login = (token, username) => ({
  type: 'LOGIN',
  token,
  username
});

export const startLogin = (username, password) => {
  return (dispatch, getState) => {

    const token = uuid();

    const user = {
      token,
      username,
      password
    };
    //save user data in localStorage
    localStorage.setItem('ADMIN_API_CARDS', JSON.stringify(user));

    dispatch(login(token, username));
  };
};

export const logout = () => ({
  type: 'LOGOUT'
});

export const startLogout = () => {
  return (dispatch, getState) => {

    //remove user data in localStorage
    localStorage.removeItem('ADMIN_API_CARDS');

    dispatch(logout());

  };
};