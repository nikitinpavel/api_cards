//public API list in array
import listApi from '../api/listApi';


// get n items from array in random order
const getRandom = (arr, n) => {
  const result = new Array(n)
    let len = arr.length;
    let  taken = new Array(len);
  if (n > len)
    n = len;
  while (n--) {
      let x = Math.floor(Math.random() * len);
      result[n] = arr[x in taken ? taken[x] : x];
      taken[x] = --len in taken ? taken[len] : len;
  }
  return result;
}

// SET_CARDS
export const setCards = (cards) => ({
  type: 'SET_CARDS',
  cards
});

export const startSetCards = () => {
  return (dispatch, getState) => {

    const randomCards = getRandom(listApi, 6);

    dispatch(setCards(randomCards));
  };
};

//GET_REQ_DATA_SUCCESS
export const getReqDataSuccess= (data) => ({
  type: 'GET_REQ_DATA_SUCCESS',
  data
});

//GET_REQ_DATA
export const getReqData = () => ({
  type: 'GET_REQ_DATA',
});

export const startGetReqData = (url, getData) => {
  return (dispatch, getState, axios) => {
    //cancel previous request if there is it, to prevent load incorrect data
    const CancelToken = axios.CancelToken;
    let cancel;
    cancel && cancel();

    dispatch(getReqData());
    axios.get(url, {
      CancelToken: new CancelToken(function executor(c) {
        cancel = c;
      })
    })
      .then((res) => getData(res.data))
      .then((data) => {
        dispatch(getReqDataSuccess(data));
        //save data request to use in case of request page reload
        sessionStorage.setItem('lastRequestData', JSON.stringify(data));
      })
      .catch((e) => console.log(e));
  }
}