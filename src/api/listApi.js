//getData is method to get text from specific API

export default [
  {
    id: 0,
    url: 'https://ghibliapi.herokuapp.com/films',
    description: 'Информация об аниме фильмах',
    getData: (data) => {
      return data.map((item) => {
        return {
          title: item.title,
          description: item.description
        }
      })
    }
  },
  {
    id: 1,
    url: 'https://api.publicapis.org/entries',
    description: 'Список публичных API',
    getData: (data) => {
      return data.entries.map((item) => {
        return {
          title: item.API,
          description: item.Description
        }
      })
    }
  },
  {
    id: 2,
    url: 'https://api.punkapi.com/v2/beers',
    description: 'Рецепты пива',
    getData: (data) => {
      return data.map((item) => {
        return {
          title: item.name,
          description: item.description
        }
      })
    }
  },
  {
    id: 3,
    url: 'https://thereportoftheweek-api.herokuapp.com/reports',
    description: 'Видео о питании',
    getData: (data) => {
      return data.map((item) => {
        return {
          title: item.videoTitle,
          description: item.product
        }
      })
    }
  },
  {
    id: 4,
    url: 'http://api.icndb.com/jokes/random/10',
    description: 'Шутки о Чаке Норрисе',
    getData: (data) => {
      return data.value.map((item) => {
        return {
          title: '',
          description: item.joke
        }
      })
    }
  },
  {
    id: 5,
    url: 'https://pokeapi.co/api/v2/pokemon',
    description: 'Информация о покемонах',
    getData: (data) => {
      return data.results.map((item) => {
        return {
          title: item.name,
          description: ''
        }
      })
    }
  },
  {
    id: 6,
    url: 'http://universities.hipolabs.com/search?name=middle',
    description: 'Информация об университетах в мире',
    getData: (data) => {
      return data.map((item) => {
        return {
          title: item.name,
          description: item.country
        }
      })
    }
  },
  {
    id: 7,
    url: 'https://www.scorebat.com/video-api/v1/',
    description: 'Видео о футболе',
    getData: (data) => {
      return data.map((item) => {
        return {
          title: item.title,
          description: item.competition.name
        }
      })
    }
  }
]