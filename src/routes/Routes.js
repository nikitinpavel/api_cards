import React from 'react';
import { BrowserRouter, Switch} from 'react-router-dom';

import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';

import LoginPage from '../compontens/LoginPage/LoginPage';
import DashPage from '../compontens/DashPage/DashPage';
import CardPage from '../compontens/CardPage/CardPage';
import SettingsPage from '../compontens/SettingsPage/SettingsPage';

// The Routing Component providing all the routing Configuration
const Routes = (props) => {
  return (
    <BrowserRouter>
      <Switch>
        <PrivateRoute exact path="/" component={DashPage} />
        <PublicRoute path="/login" component={LoginPage} />
        <PrivateRoute path="/settings" component={SettingsPage} />
        <PrivateRoute path="/card/:id" component={CardPage} />
      </Switch>
    </BrowserRouter>
  )
}

export { Routes }