import React from 'react';
import { Routes } from './routes/Routes';
import { Provider } from 'react-redux';
import { configureStore } from './store/configureStore';

import { startSetUsers } from './actions/usersActions';
import { startSetCards } from './actions/cardsActions';
import { getReqDataSuccess } from './actions/cardsActions';
import { login } from './actions/authActions';



const store = configureStore();

//get login data from localStorage
const user = JSON.parse(localStorage.getItem("ADMIN_API_CARDS"));

 if (user) {
   store.dispatch(login(user.token, user.username));
}

//fill redux state with data from last request to prevent empty page with page reload
const lastRequestData = JSON.parse(sessionStorage.getItem('lastRequestData'));
if (!!lastRequestData) {
  store.dispatch(getReqDataSuccess(lastRequestData));
}
//set random 6 cards with starting the App.
store.dispatch(startSetCards());


//fill redux state from sessionStorage
store.dispatch(startSetUsers());


const App = (props) => {
  return (

    <Provider store={store} >
      <Routes />
    </Provider>
  );
}

export default App;