import React, { Component } from 'react';
import { connect } from 'react-redux';

import Layout from '../Layout/Layout';
import Card from '../Card/Card';

import { startGetReqData } from '../../actions/cardsActions';


class DashPage extends Component {

  constructor(props) {
    super(props);

    this.onSelectCard = this.onSelectCard.bind(this);
  }

  onSelectCard(url, getData, id) {
    this.props.startGetReqData(url, getData);
    this.props.history.push(`card/${id}`);
  }

  render() {
    return (
      <Layout>
        <div className="cards">
          <div className="container">
            <div className="cards__row row">
              {this.props.cards.map((card, index) => {
                return (
                  <Card
                    key={index}
                    url={card.url}
                    description={card.description}
                    onSelect={() => this.onSelectCard(card.url, card.getData, card.id)}
                  />
                )
              })}
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  cards: state.cards.cards,
});

const mapDispatchToProps = (dispatch) => ({
  startGetReqData: (url , getData) => dispatch(startGetReqData(url, getData))
});


export default connect(mapStateToProps, mapDispatchToProps)(DashPage);
