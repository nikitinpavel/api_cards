import React from 'react';

import loader from '../../images/loader.gif';


const Loader = () => {
  return (
    <div className="loader">
      <img className="loader__image" src={loader} alt={"loader"} />
    </div>
  )
}

export default Loader
