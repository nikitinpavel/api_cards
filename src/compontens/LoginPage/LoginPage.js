import React, { Component } from 'react';
import { connect } from 'react-redux';

import { startLogin } from '../../actions/authActions';

class LoginPage extends Component {


  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',

    }

    this.handleChange = this.handleChange.bind(this);
    this.loginSubmit = this.loginSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });


  }

  loginSubmit(event) {
    event.preventDefault();

    const { username, password } = this.state;

    this.props.startLogin(username, password);

    this.setState({
      username: '',
      password: '',
    });

  }


  render() {
    return (
      <main className="page">
        <div className="login-form">
          <form className="login-form__form" onSubmit={this.loginSubmit}>
            <label className="login-form__label" htmlFor="user-name">
              Пользователь:
              <input
                className="login-form__field"
                type="text" name="username" id="user-name"
                required
                value={this.state.username}
                onChange={this.handleChange} placeholder="пользователь"
              />
            </label>
            <label className="login-form__label" htmlFor="password">
              Пароль:
              <input
                className="login-form__field"
                type="password" name="password"
                required
                id="password" value={this.state.password}
                onChange={this.handleChange} placeholder="пароль"
              />
            </label>
            {this.props.auth.error ? <p className="error">{this.props.auth.error}</p> : null}
            <button className="login-form__button" type="submit">войти</button>
          </form>
        </div>
      </main>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  auth: state.auth
});

const mapDispatchToProps = (dispatch) => ({
  startLogin: (username, password) => dispatch(startLogin(username, password))
});


export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);