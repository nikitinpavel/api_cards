import React, { Component } from 'react';
import { connect } from 'react-redux';
import Modal from 'react-modal';

import UserForm from '../UserForm/UserForm';
import Layout from '../Layout/Layout';

import { startEditUser } from '../../actions/usersActions';
import { startAddUser } from '../../actions/usersActions';
import { startRemoveUser } from '../../actions/usersActions';


class SettingsPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      addUserModal: false,
      editUserModal: false
    }

    this.closeAddModal = this.closeAddModal.bind(this);
    this.openAddModal = this.openAddModal.bind(this);

    this.closeEditModal = this.closeEditModal.bind(this);
    this.openEditModal = this.openEditModal.bind(this);

    this.onAddUser = this.onAddUser.bind(this);
    this.onEditUser = this.onEditUser.bind(this);
    this.onRemoveUser = this.onRemoveUser.bind(this);
  }

  onAddUser(user) {
    this.props.startAddUser(user);

    this.setState({
      addUserModal: false
    })
  }

  onEditUser(user) {
    this.props.startEditUser(user);
    
    this.setState({
      editUserModal: false
    })
  }

  onRemoveUser() {
    this.props.startRemoveUser();
  }

  closeAddModal() {
    this.setState({ addUserModal: false });
  }

  openAddModal() {
    this.setState({ addUserModal: true });
  }
  closeEditModal() {
    this.setState({ editUserModal: false });
  }

  openEditModal() {
    this.setState({ editUserModal: true });
  }


  render() {

    const activeUser = this.props.activeUser;

    return (
      <Layout>
        <div className="add-user">
          <div className="container">
            <div className="add-user__row row">
            <Modal
                isOpen={this.state.addUserModal}
                onRequestClose={this.closeAddModal}
                ariaHideApp={false}
                className="user-modal"
                overlayClassName="user-modal__overlay"
              >
                <UserForm onSubmit={this.onAddUser}/>
              </Modal>
              <button className="user-button user-button--add" onClick={this.openAddModal}>Добавить</button>
            </div>
          </div>
        </div>
        {/* Render User block if active user was selected */}
        { activeUser ? (<div className="user">
          <div className="user__container container">
            <div className="user__row row">
              <h4 className="user__firstName"><strong>Имя:</strong> {activeUser.firstName}</h4>
              <h4 className="user__lastName"><strong>Фамилия:</strong> {activeUser.lastName}</h4>
              <h4 className="user__about"><strong>О себе:</strong><br />
                <p>
                  {activeUser.about}
                </p>
              </h4>
              <Modal
                isOpen={this.state.editUserModal}
                onRequestClose={this.closeEditModal}
                ariaHideApp={false}
                className="user-modal"
                overlayClassName="user-modal__overlay"
              >
                <UserForm user={activeUser} onSubmit={this.onEditUser} />
              </Modal>
              <div className="user-buttons">
                <button className="user-button user-button--edit" onClick={this.openEditModal}>Редактировать</button>
                <button className="user-button user-button--remove" onClick={this.onRemoveUser}>Удалить</button>
              </div>
            </div>
          </div>
        </div>) : null}
      </Layout>
    )
  }
}

//get data of the active user
const mapStateToProps = (state) => ({
  activeUser: state.users.users.find((user) => user.id === state.users.userActive)
});

const mapDispatchToProps = (dispatch) => ({
  startAddUser: (user) => dispatch(startAddUser(user)),
  startEditUser: (user) => dispatch(startEditUser(user)),
  startRemoveUser: () => dispatch(startRemoveUser())
});


export default connect(mapStateToProps, mapDispatchToProps)(SettingsPage);
