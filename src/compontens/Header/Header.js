import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from "react-router-dom";

import { startSelectUser } from '../../actions/usersActions';
import { startLogout } from '../../actions/authActions';

import UserList from '../UserList/UserList';
import { ReactComponent as Users } from '../../icons/users.svg';
import { ReactComponent as Logo } from '../../icons/api.svg';
import { ReactComponent as Logout } from '../../icons/logout.svg';

const Header = (props) => {
  return (
    <>
      <header className="header-top">
        <div className="container">
          <div className="row">
            <nav className="nav-top">
              <NavLink to="/">
                <Logo className="nav-top__logo" />
              </NavLink>
              <ul className="nav-top__list">
                <li className="nav-top__item">
                  <div className="nav-top__icon">
                    <Users className="icon-users" />
                    <div className="user-list__wrap">
                      <UserList users={props.users} onSelect={props.startSelectUser}/>
                    </div>
                  </div>
                </li>
                <li className="nav-top__item" onClick={props.startLogout}>
                    <Logout className="icon-logout"/>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </header>
      <header className="header-low">
        <div className="container">
          <div className="row">
            <nav className="nav-low">
              <ul className="nav-low__list">
                <li className="nav-low__item">
                  <NavLink className="nav-low__link" activeClassName='nav-low__link--active' exact to="/">
                    главная
                 </NavLink>
                </li>
                <li className="nav-low__item">
                  <NavLink className="nav-low__link" activeClassName='nav-low__link--active' to="/settings">
                    настройки
                 </NavLink>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </header>
    </>
  )
}



const mapStateToProps = (state, ownProps) => ({
  users: state.users,
  auth: state.auth
});

const mapDispatchToProps = (dispatch) => ({
  startSelectUser: (id, location, history) => dispatch(startSelectUser(id, location, history)),
  startLogout: () => dispatch(startLogout()),

});


export default connect(mapStateToProps, mapDispatchToProps)(Header);