import React from 'react';

import { ReactComponent as CardIcon } from '../../icons/earth.svg';



const Card = (props) => {
  return (
    <div className="card" onClick={props.onSelect}>
      <div className="card__icon">
        <CardIcon/>
      </div>
      <h5 className="card__url">{props.url}</h5>
      <p className="card__description">{props.description}</p>
    </div>
  )
}

export default Card;
