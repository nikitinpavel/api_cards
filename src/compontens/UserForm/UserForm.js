import React, { Component } from 'react';


export default class UserForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      firstName: props.user ? props.user.firstName : '',
      lastName: props.user ? props.user.lastName : '',
      about: props.user ? props.user.about : ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });

  }

  handleSubmit(event) {
    event.preventDefault();
    const user = this.state;

    this.props.onSubmit(user);

    this.setState({
      firstName: '',
      lastName: '',
      about: '',
      modalIsOpen: false
    });
  }


  render() {
    return (
      <div className="user-form__wrap">
        <form className="user-form" onSubmit={this.handleSubmit}>
          <label className="user-form__label" htmlFor="firstName">
            Имя:<br />
            <input
              className="user-form__field"
              id="firstName"
              name="firstName" value={this.state.firstName}
              onChange={this.handleChange}
              required
            />
          </label>
          <label className="user-form__label" htmlFor="lastName">
            Фамилия:<br />
            <input
              className="user-form__field"
              id="lastName"
              name="lastName" value={this.state.lastName}
              onChange={this.handleChange}
              required
            />
          </label>
          <label className="user-form__label" htmlFor="about">
            О себе:<br />
            <textarea
              className="user-form__field user-form__field--textarea"
              required
              name="about" value={this.state.about}
              onChange={this.handleChange} cols="30" rows="6"
            />
          </label>
          <button className="button user-form__button" type="submit">Сохранить</button>
        </form>
      </div>
    )
  }
}
