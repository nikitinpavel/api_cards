import React from 'react';
import { withRouter } from 'react-router-dom';


const UserList = (props) => {
  const { userActive, users } = props.users;
  
  return (
    <ul className="user-list">
      {users.map((user, index) => {
        return (
          <li
            key={index}
            onClick={() => props.onSelect(user.id, props.location, props.history)}
            className={user.id === userActive ? 'user-list__item user-list__item--active' : 'user-list__item' }
          >
             {user.firstName}  {user.lastName}
          </li>
        )
      })}
    </ul>
  )
}

export default withRouter(UserList);
