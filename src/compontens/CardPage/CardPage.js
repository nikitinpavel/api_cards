import React from 'react';
import { connect } from 'react-redux';

import Layout from '../Layout/Layout';
import Loader from '../Loader/Loader';

const CardPage = (props) => {
  return (
    <Layout>
      <div className="card-page">
        <div className="container">
          <div className="card-page__row row">
            {props.cards.loading ? <Loader /> :
              <ul className="card-page__list">
                {
                  props.cards.dataRequest.map((item, index) => {
                    return (
                      <li key={index} className="req-item">
                        {!!item.title ? <p className="req-item__title">{item.title}</p> : null}
                        {!!item.description ? <p className="req-item__description">{item.description}</p> : null}
                      </li>
                    )
                  })
                }
              </ul>
            }
          </div>
        </div>
      </div>
    </Layout>
  )
}

const mapStateToProps = (state, ownProps) => ({
  cards: state.cards
});


export default connect(mapStateToProps, null)(CardPage);
