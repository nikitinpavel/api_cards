const usersDefaultState = {
  users: [],
  userActive: ' '
};


export function usersReducer(state = usersDefaultState, action) {

  switch (action.type) {

    case 'ADD_USER':
      return {
        ...state,
        users: [
          ...state.users,
          action.user
        ]
      };

    case 'SET_USERS':
      return {
        ...state,
        ...action.users
      };

    case 'SELECT_USER':
      return {
        ...state,
        userActive: action.id
      };

    case 'EDIT_USER':
      return {
        ...state,
        users: state.users.map((user) => {
          if (user.id === action.userId) {
            return {
              ...user,
              ...action.user
            };
          } else {
            return user;
          }
        })
      }

    case 'REMOVE_USER':
      return {
        ...state,
        users: state.users.filter(({ id }) => id !== action.userId)
      }


    default:
      return state
  }
}