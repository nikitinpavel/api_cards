import { combineReducers } from 'redux';
import { authReducer } from './authReducer';
import { usersReducer } from './usersReducer';
import { cardsReducer } from './cardsReducer';


const rootReducer = combineReducers({
  auth: authReducer,
  users: usersReducer,
  cards: cardsReducer
})

export default rootReducer