
const cardsDefaultState = {
  cards: [],
  dataRequest: [],
  loading: false
}

export function cardsReducer(state = cardsDefaultState, action) {

  switch (action.type) {

    case 'SET_CARDS':
      return {
        ...state,
        cards: [
          ...action.cards
        ]
      }
    
    case 'GET_REQ_DATA_SUCCESS':
      return {
        ...state,
        dataRequest: [...action.data],
        loading: false
      }
    
    case 'GET_REQ_DATA':
      return {
        ...state,
        loading: true
      }

    default:
      return state
  }
}
